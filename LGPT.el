;; This is Lain's Goal Plot Tracker (LGPT) not to be confused with LGBT. LGBT is gay and LGPT is not.
;; You can do what ever the hell you want with this code. It's not anything amazing.
;; You put numbers in and it makes a plot. This is so you can keep track of your goals better.

;; defining "lgpt". This way the user can enter the data with the command "lgpt". "lgpt" will open the data file then ask the user for the data and put that data in to the data file.
(defun lgpt ()
  (find-file ".data")
  (interactive)
  (insert "
" (read-string "Date (with out '/') and numbers:")))
;; "lgpt-plot" will make a plot. It does this with a GNUplot script called GUNP (Goal Number Unit Plotter)
(defun lgpt-plot ()
  (interactive)
  (shell-command "gnuplot GNUP")
  (find-file "/tmp/plot.png"))